package com.kj.apps.htg_smartroom.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.kj.apps.htg_smartroom.model.Device;
import com.kj.apps.htg_smartroom.model.Room;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by graze on 03/07/2017.
 */

public class DatabaseHelper {
    private Context context;
    private String path;

    private static DatabaseHelper instance = null;

    public static DatabaseHelper getInstance(Context context, String path) {
        if (instance == null) {
            instance = new DatabaseHelper(context, path);
        }
        return instance;
    }

    private DatabaseHelper(Context context, String path) {
        this.context = context;
        this.path = path;
    }

    private SQLiteDatabase openDB() {
        return SQLiteDatabase.openDatabase(path,
                null, SQLiteDatabase.OPEN_READWRITE);
    }

    public List<Room> getAllRooms() {
        SQLiteDatabase sqLiteDatabase = openDB();
        Cursor cursor = sqLiteDatabase.query(Room.TABLE_NAME, null, null, null, null, null, null);
        List<Room> rooms = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Room room = new Room();
                    room.setId(cursor.getInt(cursor.getColumnIndex(Room.COL_ROOM_ID)));
                    room.setRoomTopic(cursor.getString(cursor.getColumnIndex(Room.COL_ROOM_TOPIC)));
                    room.setName(cursor.getString(cursor.getColumnIndex(Room.COL_ROOM_NAME)));
                    room.setTemp(cursor.getInt(cursor.getColumnIndex(Room.COL_ROOM_TEMP)));
                    room.setHumidity(cursor.getInt(cursor.getColumnIndex(Room.COL_ROOM_HUMIDITY)));
                    room.setDevices(getRoomDevices(room.getRoomTopic()));
                    rooms.add(room);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
            sqLiteDatabase.close();
        }
        return rooms;
    }

    public Room getSingleRoom(String topic) {
        SQLiteDatabase sqLiteDatabase = openDB();
        Cursor cursor = sqLiteDatabase.query(Room.TABLE_NAME, null, Room.COL_ROOM_TOPIC + "= ?", new String[]{topic}, null, null, null);
        Room room = new Room();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    room.setId(cursor.getInt(cursor.getColumnIndex(Room.COL_ROOM_ID)));
                    room.setRoomTopic(cursor.getString(cursor.getColumnIndex(Room.COL_ROOM_TOPIC)));
                    room.setName(cursor.getString(cursor.getColumnIndex(Room.COL_ROOM_NAME)));
                    room.setTemp(cursor.getInt(cursor.getColumnIndex(Room.COL_ROOM_TEMP)));
                    room.setHumidity(cursor.getInt(cursor.getColumnIndex(Room.COL_ROOM_HUMIDITY)));
                    room.setDevices(getRoomDevices(room.getRoomTopic()));
                }
                while (cursor.moveToNext());
            }
            cursor.close();
            sqLiteDatabase.close();
        }
        return room;
    }

    public List<Device> getRoomDevices(String roomTopic) {
        SQLiteDatabase sqLiteDatabase = openDB();
        Cursor cursor = sqLiteDatabase.query(Device.TABLE_NAME, null, Device.COL_ROOM_TOPIC + "= ?", new String[]{roomTopic}, null, null, null);
        List<Device> devices = new ArrayList<>();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    Device device = new Device();
                    device.setTopic(cursor.getString(cursor.getColumnIndex(Device.COL_DEVICE_TOPIC)));
                    device.setName(cursor.getString(cursor.getColumnIndex(Device.COL_DEVICE_NAME)));
                    device.setType(cursor.getInt(cursor.getColumnIndex(Device.COL_DEVICE_TYPE)));
                    device.setState(cursor.getInt(cursor.getColumnIndex(Device.COL_DEVICE_STATE)));
                    devices.add(device);
                }
                while (cursor.moveToNext());
            }
            cursor.close();
            sqLiteDatabase.close();
        }
        return devices;
    }

    public Device getSingleDevice(String roomTopic, String deviceTopic) {
        SQLiteDatabase sqLiteDatabase = openDB();
        Cursor cursor = sqLiteDatabase.query(Device.TABLE_NAME, null, Device.COL_ROOM_TOPIC + "= ? and "
                + Device.COL_DEVICE_TOPIC + "= ?", new String[]{roomTopic, deviceTopic}, null, null, null);
        Device device = new Device();
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    device.setTopic(cursor.getString(cursor.getColumnIndex(Device.COL_DEVICE_TOPIC)));
                    device.setName(cursor.getString(cursor.getColumnIndex(Device.COL_DEVICE_NAME)));
                    device.setType(cursor.getInt(cursor.getColumnIndex(Device.COL_DEVICE_TYPE)));
                    device.setState(cursor.getInt(cursor.getColumnIndex(Device.COL_DEVICE_STATE)));
                }
                while (cursor.moveToNext());
            }
            cursor.close();
            sqLiteDatabase.close();
        }
        return device;
    }

    public String[] getAllTopics() {
        List<Room> rooms = getAllRooms();
        int i;
        int size = rooms.size();
        String[] arrTopics = new String[size];
        for (i = 0; i < size; i++) {
            arrTopics[i] = rooms.get(i).getRoomTopic() + "#";
        }
        return arrTopics;
    }

    public String[] getNotifiMessage(String roomTopic) {
        Room r = getSingleRoom(roomTopic);
        return new String[]{r.getId() + "", r.getName() + ": Motion detected!"};
    }

    public boolean shouldUpdateTheTemp(String roomTopic, int newTemp) {
        return getSingleRoom(roomTopic).getTemp() == newTemp ? false : true;
    }

    public boolean shouldUpdateTheHumidity(String roomTopic, int newHumidity) {
        return getSingleRoom(roomTopic).getHumidity() == newHumidity ? false : true;
    }

    public boolean shouldUpdateTheState(String roomTopic, String deviceTopic, int newState) {
        return getSingleDevice(roomTopic, deviceTopic).getState() == newState ? false : true;
    }

    public long updateRoomTemperature(String roomTopic, int newTemp) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Room.COL_ROOM_TEMP, newTemp);
        SQLiteDatabase database = openDB();
        long id = database.update(Room.TABLE_NAME, contentValues, Room.COL_ROOM_TOPIC + "= ?", new String[]{roomTopic});
        database.close();
        return id;
    }

    public long updateDeviceState(String roomTopic, String deviceTopic, int newState) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Device.COL_DEVICE_STATE, newState);
        SQLiteDatabase database = openDB();
        long id = database.update(Device.TABLE_NAME, contentValues,
                Device.COL_ROOM_TOPIC + "= ? and " + Device.COL_DEVICE_TOPIC + "= ?", new String[]{roomTopic, deviceTopic});
        database.close();
        return id;
    }

    public long updateRoomHumidity(String roomTopic, int newHumidity) {
        ContentValues contentValues = new ContentValues();
        contentValues.put(Room.COL_ROOM_HUMIDITY, newHumidity);
        SQLiteDatabase database = openDB();
        long id = database.update(Room.TABLE_NAME, contentValues, Room.COL_ROOM_TOPIC + "= ?", new String[]{roomTopic});
        database.close();
        return id;
    }
}
