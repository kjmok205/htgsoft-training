package com.kj.apps.htg_smartroom;

import android.content.res.ColorStateList;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Build;
import android.os.CountDownTimer;
import android.support.design.widget.TabLayout;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import com.kj.apps.htg_smartroom.adapter.RoomFragmentAdapter;
import com.kj.apps.htg_smartroom.connection.MQTTConnector;
import com.kj.apps.htg_smartroom.database.DatabaseHelper;
import com.kj.apps.htg_smartroom.model.Device;
import com.kj.apps.htg_smartroom.model.Room;

import java.util.List;

import at.grabner.circleprogress.CircleProgressView;

public class RoomActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelper;

    public static final String ROOM_TOPIC = "room_topic";
    private TextView tvTitle;
    private TextView tvAppName;
    private TextView tvTemp;
    private TextView tvHumidity;
    private TextView tvDegree;
    private CircleProgressView circleProgressView;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private String roomTopic;
    private Room room;
    private MQTTConnector connector;
    private int currentHumi = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_room);
        connector = MQTTConnector.getInstance(getApplicationContext());
        connector.attachActivity(this);
        addControls();
    }

    private void addControls() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolBar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        circleProgressView = (CircleProgressView) findViewById(R.id.circleProgress);
        tvTemp = (TextView) findViewById(R.id.tvTemp);
        tvDegree = (TextView) findViewById(R.id.tvDegree);
        tvTitle = (TextView) findViewById(R.id.tvTitle);
        tvHumidity = (TextView) findViewById(R.id.tvHumidity);
        tvAppName = (TextView) findViewById(R.id.tvAppName);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);

        databaseHelper = DatabaseHelper.getInstance(this, MQTTConnector.PATH);
        roomTopic = (String) getIntent().getExtras().getSerializable(ROOM_TOPIC);
        room = databaseHelper.getSingleRoom(roomTopic);
        currentHumi = room.getHumidity();

        Typeface font = Typeface.createFromAsset(getAssets(), "Comfortaa.ttf");
        tvAppName.setTypeface(font);
        tvAppName.setText(room.getName());

        Typeface font2 = Typeface.createFromAsset(getAssets(), "JosefinSans.ttf");
        tvTemp.setTypeface(font);
        tvTitle.setTypeface(font);
        tvHumidity.setTypeface(font);
        tvDegree.setTypeface(font2);

        tvTemp.setText(room.getTemp() + "");
        tvHumidity.setText(String.format(getString(R.string.humdity), String.valueOf(room.getHumidity())));

        viewPager.setAdapter(new RoomFragmentAdapter(getSupportFragmentManager(), room));
        tabLayout.setupWithViewPager(viewPager);
        addIconForTabs(room.getDevices());
    }

    public void addIconForTabs(List<Device> list) {
        int allTabs = tabLayout.getTabCount();

        if (allTabs <= 5) {
            tabLayout.setTabMode(TabLayout.MODE_FIXED);
        } else {
            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        }

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            if (list.get(i).getType() == Device.TYPE_BULB) {
                if (list.get(i).getState() == Device.STATE_OFF)
                    tab.setIcon(R.drawable.bulb_off);
                else
                    tab.setIcon(R.drawable.bulb_on);
            } else if (list.get(i).getType() == Device.TYPE_FAN) {
                if (list.get(i).getState() == Device.STATE_OFF)
                    tab.setIcon(R.drawable.fan_off);
                else
                    tab.setIcon(R.drawable.fan_on);
            }
            setSelectedEventForTabLayout(tab);
        }
    }

    private void setSelectedEventForTabLayout(TabLayout.Tab tab) {
        ColorStateList colors;
        if (Build.VERSION.SDK_INT >= 23) {
            colors = getResources().getColorStateList(R.color.tab_icon, getTheme());
        } else {
            colors = getResources().getColorStateList(R.color.tab_icon);
        }
        Drawable icon = tab.getIcon();
        if (icon != null) {
            icon = DrawableCompat.wrap(icon);
            DrawableCompat.setTintList(icon, colors);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            this.onBackPressed();
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        connector.showToast(true);
        new TempRunningTask().execute(Integer.parseInt(tvTemp.getText().toString()), room.getTemp());
    }

    @Override
    protected void onPause() {
        super.onPause();
        connector.showToast(false);
    }

    public void notifyChanging(String roomTopic) {
        if (this.roomTopic.equals(roomTopic)) {
            room = databaseHelper.getSingleRoom(this.roomTopic);
            viewPager.getAdapter().notifyDataSetChanged();
            addIconForTabs(room.getDevices());
            addIconForTabs(room.getDevices());
        }
    }

    public void turnOnOffDevice(String deviceTopic, int state) {
        databaseHelper.updateDeviceState(roomTopic, deviceTopic, state);
        notifyChanging(roomTopic);
        connector.publishTheMessage(roomTopic + deviceTopic, state + "");
    }

    public void setCurrentTemp(String roomTopic, int newTemp) {
        if (this.roomTopic.equals(roomTopic)) {
            new TempRunningTask().execute(Integer.parseInt(tvTemp.getText().toString()), newTemp);
        }

    }

    public void setCurrentHumidity(String roomTopic, int newHumidity) {
        if (this.roomTopic.equals(roomTopic)) {
            new HumiRunningTask().execute(currentHumi, newHumidity);
        }
    }

    private class TempRunningTask extends AsyncTask<Integer, Integer, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            try {
                if (params[0] > params[1]) {
                    for (int i = params[0]; i >= params[1]; i--) {
                        publishProgress(i);
                        Thread.sleep(8);
                    }
                } else {
                    for (int i = params[0]; i <= params[1]; i++) {
                        publishProgress(i);
                        Thread.sleep(8);
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            circleProgressView.setValue(values[0]);
            tvTemp.setText(values[0] + "");
        }
    }

    private class HumiRunningTask extends AsyncTask<Integer, Integer, Void> {

        @Override
        protected Void doInBackground(Integer... params) {
            try {
                if (params[0] > params[1]) {
                    for (int i = params[0]; i >= params[1]; i--) {
                        publishProgress(i);
                        Thread.sleep(10);
                    }
                } else {
                    for (int i = params[0]; i <= params[1]; i++) {
                        publishProgress(i);
                        Thread.sleep(10);
                    }
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            currentHumi = values[0];
            tvHumidity.setText(String.format(getString(R.string.humdity), String.valueOf(values[0])));
        }
    }
}
