package com.kj.apps.htg_smartroom.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.ImageView;
import android.widget.TextView;

import com.kj.apps.htg_smartroom.R;
import com.kj.apps.htg_smartroom.RoomActivity;
import com.kj.apps.htg_smartroom.model.Room;

import java.util.List;

/**
 * Created by graze on 27/06/2017.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.MainViewHolder> {

    private Context mContext;
    private int mLayout;
    private List<Room> listRoom;

    public MainAdapter(Context mContext, int mLayout, List<Room> listRoom) {
        this.mContext = mContext;
        this.mLayout = mLayout;
        this.listRoom = listRoom;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = layoutInflater.inflate(mLayout, parent, false);
        return new MainViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MainViewHolder holder, final int position) {
        Typeface typeface = Typeface.createFromAsset(mContext.getAssets(), "Comfortaa.ttf");
        holder.tvRoom.setTypeface(typeface);
        holder.tvTotal.setTypeface(typeface);
        holder.tvRoom.setText(listRoom.get(position).getName());
        Log.i("test", "onBindViewHolder: " + listRoom.get(position).getTemp());
        holder.tvTotal.setText(mContext.getString(R.string.total_devices, listRoom.get(position).getDevices().size() + ""));

        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext, RoomActivity.class);
                intent.putExtra(RoomActivity.ROOM_TOPIC, listRoom.get(position).getRoomTopic());
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listRoom.size();
    }


    class MainViewHolder extends RecyclerView.ViewHolder {

        TextView tvRoom;
        TextView tvTotal;
        CardView cardView;

        public MainViewHolder(View itemView) {
            super(itemView);
            tvRoom = (TextView) itemView.findViewById(R.id.tvRoom);
            tvTotal = (TextView) itemView.findViewById(R.id.tvTotal);
            cardView = (CardView) itemView.findViewById(R.id.cardView);
        }
    }
}