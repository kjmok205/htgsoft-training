package com.kj.apps.htg_smartroom.model;

import java.io.Serializable;

/**
 * Created by graze on 22/06/2017.
 */

public class Device implements Serializable {

    public static String TABLE_NAME = "device";
    public static String COL_ID = "id";
    public static String COL_ROOM_TOPIC = "room_topic";
    public static String COL_DEVICE_TOPIC = "device_topic";
    public static String COL_DEVICE_NAME = "device_name";
    public static String COL_DEVICE_TYPE = "device_type";
    public static String COL_DEVICE_STATE = "device_state";

    public static final int TYPE_BULB = 1;
    public static final int TYPE_FAN = 0;

    public static final int STATE_ON = 1;
    public static final int STATE_OFF = 0;

    private String topic;
    private String name;
    private int type;
    private int state = 0;

    public Device() {
    }

    public Device(String name, int type, String topic) {
        this.topic = topic;
        this.name = name;
        this.type = type;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
