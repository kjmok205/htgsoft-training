package com.kj.apps.htg_smartroom.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.ViewGroup;

import com.kj.apps.htg_smartroom.fragment.RoomFragment;
import com.kj.apps.htg_smartroom.model.Room;


/**
 * Created by graze on 27/06/2017.
 */
public class RoomFragmentAdapter extends FragmentStatePagerAdapter {

    private Room room;

    public RoomFragmentAdapter(FragmentManager fm, Room room) {
        super(fm);
        this.room = room;
    }

    @Override
    public Fragment getItem(int position) {
        return RoomFragment.newInstance(room.getDevices().get(position).getTopic(), room.getRoomTopic());
    }

    @Override
    public int getCount() {
        return room.getDevices().size();
    }

    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    @Override
    public void finishUpdate(ViewGroup container) {
        try {
            super.finishUpdate(container);
        } catch (NullPointerException nullPointerException) {
            System.out.println("Catch the NullPointerException in FragmentPagerAdapter.finishUpdate");
        }
    }
}
