package com.kj.apps.htg_smartroom;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.kj.apps.htg_smartroom.connection.MQTTConnector;

/**
 * Created by graze on 29/06/2017.
 */

public class NetworkReceiver extends BroadcastReceiver {

    private MQTTConnector connector;

    public NetworkReceiver(MQTTConnector connector) {
        this.connector = connector;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (isHasNetwork(context)) {
            if (!connector.isConnected()) {
                connector.getConnect();
            }
        } else {
            if (connector.show)
                Toast.makeText(context, "Device is working offline.", Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean isHasNetwork(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return (netInfo != null && netInfo.isConnected());
    }
}
