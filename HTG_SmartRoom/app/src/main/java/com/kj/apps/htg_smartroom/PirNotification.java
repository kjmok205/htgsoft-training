package com.kj.apps.htg_smartroom;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;

/**
 * Created by graze on 29/06/2017.
 */

public class PirNotification {
    private static int MY_REQUEST_CODE = 1;

    public static void postNotification(Context context, String[] mess) {
        NotificationCompat.Builder notBuilder = new NotificationCompat.Builder(context);
        notBuilder.setTicker("This is a ticker");
        notBuilder.setSmallIcon(R.drawable.warning);
        notBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.drawable.app_logo));
        notBuilder.setContentTitle("Smart room: WARNING!!!");
        notBuilder.setContentText(mess[1]);
        notBuilder.setDefaults(Notification.DEFAULT_ALL);
        notBuilder.setPriority(Notification.PRIORITY_HIGH);
        notBuilder.setVibrate(new long[]{100, 250, 100, 500});
        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        notBuilder.setSound(alarmSound);

        Intent intent = new Intent(context, MainActivity.class);

        PendingIntent pendingIntent = PendingIntent.getActivity(context, MY_REQUEST_CODE,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notBuilder.setContentIntent(pendingIntent);

        NotificationManager notificationService =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = notBuilder.build();
        notificationService.notify(Integer.parseInt(mess[0]), notification);
    }
}
