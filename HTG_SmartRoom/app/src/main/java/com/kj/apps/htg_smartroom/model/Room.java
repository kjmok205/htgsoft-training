package com.kj.apps.htg_smartroom.model;

import java.io.Serializable;
import java.util.List;

/**
 * Created by graze on 26/06/2017.
 */

public class Room implements Serializable {

    //DATABASE INFO DEFINE
    public static String TABLE_NAME = "room";
    public static String COL_ROOM_ID = "room_id";
    public static String COL_ROOM_TOPIC = "room_topic";
    public static String COL_ROOM_NAME = "room_name";
    public static String COL_ROOM_TEMP = "room_temp";
    public static String COL_ROOM_HUMIDITY = "room_humidity";

    private int id;
    private int temp = 0;
    private String roomTopic;
    private String name;
    private List<Device> devices;
    private int humidity = 0;

    public Room(String roomTopic, String name, List<Device> devices) {
        this.name = name;
        this.roomTopic = roomTopic;
        this.devices = devices;
    }

    public Room() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getHumidity() {
        return humidity;
    }

    public void setHumidity(int humidity) {
        this.humidity = humidity;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public void setDevices(List<Device> devices) {
        this.devices = devices;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRoomTopic() {
        return roomTopic;
    }

    public void setRoomTopic(String roomTopic) {
        this.roomTopic = roomTopic;
    }

    public int getTemp() {
        return temp;
    }

    public void setTemp(int temp) {
        this.temp = temp;
    }

}
