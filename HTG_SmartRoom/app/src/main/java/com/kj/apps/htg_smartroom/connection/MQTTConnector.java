package com.kj.apps.htg_smartroom.connection;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;

import com.kj.apps.htg_smartroom.NetworkReceiver;
import com.kj.apps.htg_smartroom.PirNotification;
import com.kj.apps.htg_smartroom.RoomActivity;
import com.kj.apps.htg_smartroom.database.DatabaseHelper;

import org.eclipse.paho.android.service.MqttAndroidClient;
import org.eclipse.paho.client.mqttv3.IMqttActionListener;
import org.eclipse.paho.client.mqttv3.IMqttDeliveryToken;
import org.eclipse.paho.client.mqttv3.IMqttToken;
import org.eclipse.paho.client.mqttv3.MqttCallback;
import org.eclipse.paho.client.mqttv3.MqttClient;
import org.eclipse.paho.client.mqttv3.MqttConnectOptions;
import org.eclipse.paho.client.mqttv3.MqttException;
import org.eclipse.paho.client.mqttv3.MqttMessage;
import org.eclipse.paho.client.mqttv3.MqttSecurityException;
import org.eclipse.paho.client.mqttv3.persist.MemoryPersistence;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by graze on 29/06/2017.
 */

public class MQTTConnector {
    String TAG = "test";
    public static String PATH;
    private Context context;
    private MqttAndroidClient androidClient;
    private MqttConnectOptions connectOptions;
    private DatabaseHelper databaseHelper;
    private RoomActivity activity;
    public static boolean show = true;

    private boolean updateHumiDone = true;
    private boolean updateTempDone = true;
    private MemoryPersistence memoryPersistence;

    private static MQTTConnector instance = null;

    public static MQTTConnector getInstance(Context context) {
        return instance == null ? instance = new MQTTConnector(context) : instance;
    }

    private MQTTConnector(Context context) {
        this.context = context;
        initDB();
        memoryPersistence = new MemoryPersistence();
        androidClient = new MqttAndroidClient(context, ServerConfig.getUri(), ServerConfig.CLIENT_ID, memoryPersistence);
        connectOptions = new MqttConnectOptions();
        connectOptions.setUserName("phuong");
        connectOptions.setPassword("phuong".toCharArray());
        connectOptions.setAutomaticReconnect(false);
        connectOptions.setCleanSession(true);
        connectOptions.setConnectionTimeout(5);
        setCallback();
    }

    private void initDB() {
        PATH = context.getFilesDir().getAbsolutePath() + "/database";
        File file = new File(PATH);
        if (!file.exists()) {
            AssetManager assetManager = context.getAssets();
            try {
                BufferedInputStream bis = new BufferedInputStream(assetManager.open("database"));
                FileOutputStream fos = context.openFileOutput("database", Context.MODE_PRIVATE);

                byte[] buffer = new byte[512];
                int i = 0;
                while ((i = bis.read(buffer)) != -1) {
                    fos.write(buffer, 0, i);
                }

                bis.close();
                fos.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        databaseHelper = DatabaseHelper.getInstance(context, PATH);
    }

    public void showToast(boolean isShow) {
        show = isShow;
    }

    public void attachActivity(RoomActivity activity) {
        this.activity = activity;
    }

    public void setCallback() {
        if (androidClient != null) {
            androidClient.setCallback(new MqttCallback() {

                @Override
                public void connectionLost(Throwable cause) {
                    if (NetworkReceiver.isHasNetwork(context)) {
                        if (show)
                            Toast.makeText(context, "Lost connection to server, trying to reconnect", Toast.LENGTH_LONG).show();
                        getConnect();
                    }
                }

                @Override
                public void messageArrived(String topic, MqttMessage message) throws Exception {
                    String roomKey = topic.split("/")[0] + "/" + topic.split("/")[1] + "/";
                    String roomTopic = topic.split("/")[2];
                    int mess = Integer.parseInt(new String(message.getPayload()));
                    Log.i(TAG, "messageArrived: " + topic + ": " + mess);
                    if (roomTopic.equals("pir") && new String(message.getPayload()).equals("1")) {
                        PirNotification.postNotification(context,
                                databaseHelper.getNotifiMessage(roomKey));
                    } else if (roomTopic.equals("t") && databaseHelper.shouldUpdateTheTemp(roomKey, mess)) {
                        if (updateTempDone) {
                            Log.i(TAG, "Updated the new temp: " + databaseHelper.updateRoomTemperature(roomKey, mess));
                            activity.setCurrentTemp(roomKey, mess);
                            new CountDownTimer(3000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    updateTempDone = false;
                                }

                                public void onFinish() {
                                    updateTempDone = true;
                                }
                            }.start();
                        }
                    } else if (roomTopic.equals("h") && databaseHelper.shouldUpdateTheHumidity(roomKey, mess)) {
                        if (updateHumiDone) {
                            Log.i(TAG, "Updated the new temp: " + databaseHelper.updateRoomHumidity(roomKey, mess));
                            activity.setCurrentHumidity(roomKey, mess);
                            new CountDownTimer(3000, 1000) {
                                public void onTick(long millisUntilFinished) {
                                    updateHumiDone = false;
                                }

                                public void onFinish() {
                                    updateHumiDone = true;
                                }
                            }.start();
                        }
                    } else if (roomTopic.contains("d") && databaseHelper.shouldUpdateTheState(roomKey, roomTopic, mess)) {
                        Log.i(TAG, "Updated the new state: " + databaseHelper.updateDeviceState(roomKey, roomTopic, mess));
                        try {
                            activity.notifyChanging(roomKey);
                        } catch (Exception e) {

                        }
                    }
                }

                @Override
                public void deliveryComplete(IMqttDeliveryToken token) {

                }
            });
        }

    }

    public void getConnect() {
        if (androidClient != null) {
            try {
                androidClient.connect(connectOptions, null, new IMqttActionListener() {

                    @Override
                    public void onSuccess(IMqttToken asyncActionToken) {
                        Log.i(TAG, "Connected successful: " + ServerConfig.getUri());
                        if (show)
                            Toast.makeText(context, "Server connected", Toast.LENGTH_LONG).show();
                        subscribeToTopic();
                    }

                    @Override
                    public void onFailure(IMqttToken asyncActionToken, Throwable exception) {
                        Log.i(TAG, "Server can't connect: " + ServerConfig.getUri());
                        if (NetworkReceiver.isHasNetwork(context)) {
                            if (show)
                                Toast.makeText(context, "Failed ! Reconnecting, please wait...", Toast.LENGTH_SHORT).show();
                            getConnect();
                        } else {
                            if (show)
                                Toast.makeText(context, "Network problems can't connect", Toast.LENGTH_LONG).show();
                        }
                    }
                });
            } catch (MqttSecurityException e) {
                e.printStackTrace();
            } catch (MqttException e) {
                e.printStackTrace();
            }
        }
    }

    public void subscribeToTopic() {
        String[] arrTopics = databaseHelper.getAllTopics();
        int[] arrQos = new int[arrTopics.length];
        try {
            androidClient.subscribe(arrTopics, arrQos);
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void publishTheMessage(String topic, String mess) {
        try {
            MqttMessage mqttMessage = new MqttMessage();
            mqttMessage.setQos(0);
            mqttMessage.setRetained(true);
            mqttMessage.setPayload(mess.getBytes());
            if (androidClient != null) {
                try {
                    androidClient.publish(topic, mqttMessage);
                } catch (NullPointerException e) {

                }
            }
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public void disconnect() {
        try {
            androidClient.disconnect();
        } catch (MqttException e) {
            e.printStackTrace();
        }
    }

    public boolean isConnected() {
        try {
            if (androidClient != null && androidClient.isConnected())
                return true;
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return false;
    }
}
