package com.kj.apps.htg_smartroom;

import android.content.Context;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.kj.apps.htg_smartroom.adapter.MainAdapter;
import com.kj.apps.htg_smartroom.connection.MQTTConnector;
import com.kj.apps.htg_smartroom.database.DatabaseHelper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

public class MainActivity extends AppCompatActivity {

    private DatabaseHelper databaseHelper;

    private TextView tvAppName;
    private RecyclerView recyclerView;
    private MainAdapter mainAdapter;

    private MQTTConnector connector;
    private NetworkReceiver receiver;
    private IntentFilter filter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        addControls();
    }

    private void addControls() {
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolBar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        tvAppName = (TextView) findViewById(R.id.tvAppName);

        setSupportActionBar(mToolbar);

        Typeface font = Typeface.createFromAsset(getAssets(), "Comfortaa.ttf");
        tvAppName.setTypeface(font);

        connector = MQTTConnector.getInstance(getApplicationContext());
        receiver = new NetworkReceiver(connector);
        filter = new IntentFilter();
        filter.addAction("android.net.conn.CONNECTIVITY_CHANGE");
        filter.addAction("android.net.wifi.WIFI_STATE_CHANGED");
        registerReceiver(receiver, filter);
        databaseHelper = DatabaseHelper.getInstance(this, MQTTConnector.PATH);
        mainAdapter = new MainAdapter(this, R.layout.item_list_room, databaseHelper.getAllRooms());
        recyclerView.setAdapter(mainAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    protected void onResume() {
        super.onResume();
        connector.showToast(true);
    }

    @Override
    protected void onPause() {
        super.onPause();
        connector.showToast(false);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (connector.isConnected())
            connector.disconnect();
        try {
            unregisterReceiver(receiver);
        } catch (Exception e) {

        }
    }
}
