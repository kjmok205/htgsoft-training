package com.kj.apps.htg_smartroom.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kj.apps.htg_smartroom.RoomActivity;
import com.kj.apps.htg_smartroom.connection.MQTTConnector;
import com.kj.apps.htg_smartroom.NetworkReceiver;
import com.kj.apps.htg_smartroom.R;
import com.kj.apps.htg_smartroom.database.DatabaseHelper;
import com.kj.apps.htg_smartroom.model.Device;

import at.grabner.circleprogress.CircleProgressView;

/**
 * Created by graze on 27/06/2017.
 */

public class RoomFragment extends Fragment {

    private boolean running = false;
    private Device device;
    private TextView tvName;
    private ImageView imgPower;
    private String roomTopic;
    private CircleProgressView crcProgress;

    public static RoomFragment newInstance(String deviceTopic, String roomTopic) {
        Bundle args = new Bundle();
        args.putString("deviceTopic", deviceTopic);
        args.putString("roomTopic", roomTopic);
        RoomFragment fragment = new RoomFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String deviceTopic = getArguments().getString("deviceTopic");
        roomTopic = getArguments().getString("roomTopic");
        device = DatabaseHelper.getInstance(getContext(), MQTTConnector.PATH).getSingleDevice(roomTopic, deviceTopic);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.room_fragment, container, false);
        tvName = (TextView) view.findViewById(R.id.tvName);
        imgPower = (ImageView) view.findViewById(R.id.imgPower);
        crcProgress = (CircleProgressView) view.findViewById(R.id.crcProgress);

        tvName.setText(device.getName());
        if (device.getState() == Device.STATE_OFF) {
            imgPower.setBackgroundResource(R.drawable.power_off);
            crcProgress.setValue(0);
        } else if (device.getState() == Device.STATE_ON) {
            imgPower.setBackgroundResource(R.drawable.power_on);
            crcProgress.setValue(100);
        }

        imgPower.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (running == false)
                    if (NetworkReceiver.isHasNetwork(getContext())) {
                        if (device.getState() == Device.STATE_OFF) {
                            ((RoomActivity) getContext()).turnOnOffDevice(device.getTopic(), Device.STATE_ON);
                        } else if (device.getState() == Device.STATE_ON)
                            ((RoomActivity) getContext()).turnOnOffDevice(device.getTopic(), Device.STATE_OFF);
                    } else {
                        Toast.makeText(getContext(), "Device working offline...", Toast.LENGTH_SHORT).show();
                    }
            }
        });
        return view;
    }
}
