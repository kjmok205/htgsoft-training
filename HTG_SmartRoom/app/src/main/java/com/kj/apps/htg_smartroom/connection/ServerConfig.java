package com.kj.apps.htg_smartroom.connection;

/**
 * Created by graze on 27/06/2017.
 */

public class ServerConfig {
    //public static String HOST = "115.79.200.245";
    //public static String HOST = "192.168.1.3";
    public static String HOST = "54.254.171.121";
    //public static String HOST = "broker.mqttdashboard.com";
    public static int PORT = 1883;
    public static String CLIENT_ID = "KJ_APPS_" + System.currentTimeMillis();

    public static String getUri() {
        return "tcp://" + HOST + ":" + PORT;
    }
}
